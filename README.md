Code that demonstrates shadow mapping of a cube.
Scene contains surface, cube and light source.
Uses depth buffer to compute which point is lit.