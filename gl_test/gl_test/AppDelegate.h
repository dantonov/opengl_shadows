//
//  AppDelegate.h
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GLView.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
	NSWindow *window;
	NSOpenGLContext * glContext;
	GLView * glView;
//	NSViewController *controller;
}

@property (nonatomic, retain) NSWindow * window;
//@property (nonatomic, retain) NSViewController * controller;
@property (nonatomic, retain) NSOpenGLContext * glContext;
@property (nonatomic, retain) GLView * glView;

@end

