//
//  AppDelegate.m
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window, glContext, glView;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	NSRect rect = NSMakeRect(200, 200, 400, 300);
    self.window = [[[NSWindow alloc] initWithContentRect:rect
		styleMask:(NSClosableWindowMask | NSTitledWindowMask)
		backing:NSBackingStoreBuffered
		defer:NO] autorelease];

	self.window.backgroundColor = [NSColor redColor];
	[self.window setIsVisible: YES];
	[self.window setTitle:@"OGL"];

//	self.controller = [[[ViewController alloc] init] autorelease];
//	[self.controller setView: self.window.contentView];





    NSOpenGLPixelFormatAttribute attributes[]
        = { NSOpenGLPFADoubleBuffer, NSOpenGLPFADepthSize, 24, NSOpenGLPFAStencilSize, 8, 0 };

    NSOpenGLPixelFormat* pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];

	glContext = [[[NSOpenGLContext alloc] init] autorelease];

	NSRect winRect = [self.window frame];
	glView = [[GLView alloc] initWithFrame:winRect pixelFormat:pixelFormat];

	[glContext setView: glView];
	[glContext makeCurrentContext];

	[self.window setContentView: glView];

	[glView initGL];


	[self.window makeKeyAndOrderFront: nil];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	// Insert code here to tear down your application
}

@end
