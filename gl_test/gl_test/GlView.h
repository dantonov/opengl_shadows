//
//  GlView.h
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import <AppKit/NSOpenGLView.h>

#ifndef GlView_h
#define GlView_h

@interface GLView : NSOpenGLView
{
}

@property (nonatomic, retain) NSTimer * timer;

- (void) initGL;

- (void)reshape;
- (void)drawRect:(NSRect)bounds;
- (void)draw;

- (void) dealloc;

@end

#endif /* GlView_h */
