//
//  GlView.m
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import "GLView.h"
#include "Scene.h"

@implementation GLView

@synthesize timer;

Scene * mScene;

- (void) initGL
{
	mScene = new Scene();
	mScene->init();

	timer = [NSTimer scheduledTimerWithTimeInterval: 0.1
		target: self
		selector:@selector(draw)
		userInfo: nil repeats:YES];
}

- (void)reshape
{
//	[self draw];
}

- (void)drawRect:(NSRect)bounds
{
	if (mScene)
		mScene->setSize(bounds.size.width, bounds.size.height);

	[self draw];
}

- (void)draw
{
	if (!mScene)
		return;

	mScene->draw();
}

- (void) dealloc
{
	delete mScene;
	[super dealloc];
}

@end
