//
//  Scene.cpp
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//
#ifndef __KJSDGHLDJFKLJSDKLFJKLSDJFLK__
#define __KJSDGHLDJFKLJSDKLFJKLSDJFLK__

#include "Scene.h"
#include <iostream>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <vector>
#include <OpenGL/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Vertex.h"

static const std::string sWorkDir = "/Users/dmitryAntonov/Downloads/gl_prog/gl_test/gl_test/";
static const int sDepthBufDim = 512;

/**
	-------------
	| *			|
	|			|
	|	 OBJ	|
	|			|
	|			|
	-------------
		 CAM

* - light
CAM - camera
OBJ - object
*/

void Scene::init()
{
	// create perspective mx
	mProjMX = glm::perspective(90.0f, 4.0f / 3.0f, 1.0f, 20.0f);
	mLightProjMX = mProjMX;
	// create view mx
	mViewMX = glm::lookAt(glm::vec3(2.0f, 2.0f, 5.0f), glm::vec3(5.0f, 5.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	// Create light view mx
	mLightViewMX = glm::lookAt(glm::vec3(8.0f, 6.0f, 5.0f), glm::vec3(5.0f, 5.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
}

void Scene::setSize(int width, int height)
{
	mWidth = width;
	mHeight = height;
}

void Scene::draw()
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	setupViewport();

	if (mBoolFirstDraw)
	{
		setupSimpleProgram();
		setupAdvancedProgram();
		setupShadowProgram();
		setupObjectVBO();
		setupSurfaceVBO();
		setupFramebuffer();

		mBoolFirstDraw = false;
	}
	else
	{
		//
		glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferId);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		drawObject(mLightProjMX, mLightViewMX, mProgramSimpleId, false);
		drawSurface(mLightProjMX, mLightViewMX, mProgramSimpleId, false);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		//
//		drawFramebufferContent();
		//
		drawFinal(mProjMX, mViewMX);
	}

	glSwapAPPLE();
}

void Scene::setupViewport()
{
	glViewport(0, 0, mWidth, mHeight);
//	glMatrixMode(GL_PROJECTION);
	glEnable(GL_DEPTH_TEST);
}

void Scene::setupSimpleProgram()
{
	setupProgram(sWorkDir + "shaders/simple.vsh",
		sWorkDir + "shaders/simple.fsh",
		&mProgramSimpleId);

	checkGLError(__FUNCTION__);
}

void Scene::setupAdvancedProgram()
{
	setupProgram(sWorkDir + "shaders/advanced.vsh",
		sWorkDir + "shaders/advanced.fsh",
		&mProgramAdvancedId);

	checkGLError(__FUNCTION__);
}

void Scene::setupShadowProgram()
{
	setupProgram(sWorkDir + "shaders/shadow.vsh",
		sWorkDir + "shaders/shadow.fsh",
		&mProgramShadowId);

	checkGLError(__FUNCTION__);
}

void Scene::setupProgram(const std::string & vertFilePath, const std::string & fragFilePath, GLuint * programId)
{
	std::string vertStr = readFile(vertFilePath.c_str());
	std::string fragStr = readFile(fragFilePath.c_str());

	if (vertStr.empty() || fragStr.empty())
		return;

	//
	GLuint vertId = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragId = glCreateShader(GL_FRAGMENT_SHADER);
	//
	const GLchar * vertCharStr = vertStr.c_str();
	glShaderSource(vertId, 1, &vertCharStr, nullptr);
	const GLchar * fragCharStr = fragStr.c_str();
	glShaderSource(fragId, 1, &fragCharStr, nullptr);
	//
	glCompileShader(vertId);
	GLint vertIsCompiled = 0;
    glGetShaderiv(vertId, GL_COMPILE_STATUS, &vertIsCompiled);
    if (!vertIsCompiled) {
		printf("%s\n", printLogForShader(vertId).c_str());
        return;
    }

	glCompileShader(fragId);
	GLint fragIsCompiled = 0;
    glGetShaderiv(fragId, GL_COMPILE_STATUS, &fragIsCompiled);
    if (!fragIsCompiled) {
		printf("%s\n", printLogForShader(fragId).c_str());
        return;
    }

	*programId = glCreateProgram();
	glAttachShader(*programId, vertId);
	glAttachShader(*programId, fragId);

	glLinkProgram(*programId);
}

void Scene::setupSurfaceVBO()
{
	//
	std::vector<Vertex> vertices = {
		{0.0f, 0.0f, 0.0f, 1.0f, 1.0f},
		{10.0f, 0.0f, 0.0f, 0.0f, 1.0f},
		{0.0f, 10.0f, 0.0f, 1.0f, 0.0f},
		{10.0f, 10.0f, 0.0f, 0.0f, 0.0f}
	};
	//
	std::vector<GLuint> indices = {
		0, 1, 2,
		1, 2, 3
	};

	glGenBuffers(1, &mSurfaceVertexBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, mSurfaceVertexBufferId);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &mSurfaceIndexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mSurfaceIndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	checkGLError(__FUNCTION__);
}

void Scene::setupObjectVBO()
{
	std::vector<Vertex> vertices = {
		{-0.5f,-0.5f,-0.5f}, //0
		{0.5f, -0.5f, -0.5f},//1
		{-0.5f, 0.5f, -0.5f},//2
		{0.5f, 0.5f,-0.5f}, //3

		{0.5f,-0.5f, 0.5f},//4
		{0.5f, 0.5f, 0.5f},//5
		{-0.5f,-0.5f,0.5f},//6
		{-0.5f, 0.5f,0.5f} //7
	};
	//
	std::vector<GLuint> indices = {
		0, 1, 2,
		1, 2, 3,
		1, 4, 0,
		0, 4, 6,
		4, 6, 5,
		6, 5, 7,
		5, 7, 3,
		7, 3, 2,
		1, 4, 3,
		4, 3, 5,
		0, 2, 6,
		6, 2, 7
	};

	glGenBuffers(1, &mObjectVertexBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, mObjectVertexBufferId);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &mObjectIndexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mObjectIndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	checkGLError(__FUNCTION__);
}

void Scene::setupFramebuffer()
{
	glGenFramebuffers(1, &mFramebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferId);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	glGenTextures(1, &mFBTextureId);
	glBindTexture(GL_TEXTURE_2D, mFBTextureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, sDepthBufDim, sDepthBufDim, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mFBTextureId, 0);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
	switch(status)
	{
		case GL_FRAMEBUFFER_COMPLETE:
			break;
		case GL_FRAMEBUFFER_UNDEFINED:
			printf("GL_FRAMEBUFFER_UNDEFINED\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			printf("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			printf("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			printf("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			printf("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n");
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			printf("GL_FRAMEBUFFER_UNSUPPORTED\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			printf("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
			break;
		default:
			printf("Scene::setupFramebuffer framebuffer is not ready with other error\n");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	float txW = (float)mWidth / (float)sDepthBufDim;
	float txH = (float)mHeight / (float)sDepthBufDim;
	// VBO для отрисовки
	std::vector<Vertex> vertices = {
		{-1.0f,-1.0f, 0.0f, txW, txH},
		{ 1.0f,-1.0f, 0.0f, 0.0f, txH},
		{-1.0f, 1.0f, 0.0f, txW, 0.0f},
		{ 1.0f, 1.0f, 0.0f, 0.0f, 0.0f}
	};
	std::vector<GLuint> indices = {
		0, 1, 2,
		1, 2, 3
	};

	glGenBuffers(1, &mFBVBOId);
	glBindBuffer(GL_ARRAY_BUFFER, mFBVBOId);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &mFBVBOIntexId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mFBVBOIntexId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	checkGLError(__FUNCTION__);
}

void Scene::drawFramebufferContent()
{
	glm::mat4x4 projMx = glm::ortho(0.0f, 1.0f, 1.0f, 0.0f, 0.1f, 100.0f);
	glm::mat4x4 viewMx;
	viewMx = glm::translate(viewMx, glm::vec3(0.5f, 0.5f, -1.0f));
	glm::mat4x4 modelMx;
	modelMx = glm::scale(modelMx, glm::vec3(0.25f, 0.25f, 0.25f));

	glUseProgram(mProgramAdvancedId);

	GLint mmxLocation = glGetUniformLocation(mProgramAdvancedId, "uModelMX");
	GLint viewmxLocation = glGetUniformLocation(mProgramAdvancedId, "uViewMX");
	GLint projLocation = glGetUniformLocation(mProgramAdvancedId, "uProjMX");
	GLint addColorLocation = glGetUniformLocation(mProgramAdvancedId, "uAdditiveColor");

	glUniformMatrix4fv(mmxLocation, 1, GL_FALSE, glm::value_ptr(modelMx));
	glUniformMatrix4fv(viewmxLocation, 1, GL_FALSE, glm::value_ptr(viewMx));
	glUniformMatrix4fv(projLocation, 1, GL_FALSE, glm::value_ptr(projMx));
	glUniform4f(addColorLocation, 0.0f, 0.0f, 0.0f, 0.0f);

	glBindBuffer(GL_ARRAY_BUFFER, mFBVBOId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mFBVBOIntexId);
	glBindTexture(GL_TEXTURE_2D, mFBTextureId);

    glActiveTexture(GL_TEXTURE0);

    GLint vertLocation = glGetAttribLocation(mProgramAdvancedId, "aPosition");
	glVertexAttribPointer(vertLocation, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

    GLint texLocation = glGetAttribLocation(mProgramAdvancedId, "aTextureCoord");
	uint64_t txOffset = offsetof(Vertex, tx);
	glVertexAttribPointer(texLocation, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void *)txOffset);

	glEnableVertexAttribArray(vertLocation);
	glEnableVertexAttribArray(texLocation);
	//
	glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, 0);
	//
	glDisableVertexAttribArray(vertLocation);
	glDisableVertexAttribArray(texLocation);

	checkGLError(__FUNCTION__);
}

glm::mat4x4 Scene::getSurfaceMX()
{
	static float angleS = 0.0f;

	glm::mat4x4 modelMx;
	modelMx = glm::translate(modelMx, glm::vec3(5.0f, 5.0f, 0.0f));
	modelMx = glm::rotate(modelMx, angleS, glm::vec3(0.0f, 0.0f, 1.0f));
	modelMx = glm::translate(modelMx, glm::vec3(-5.0f,-5.0f, -0.0f));

	angleS += 0.1f;

	return modelMx;
}

glm::mat4x4 Scene::getObjectMX()
{
	static float angleO = 0.0f;

	glm::mat4x4 modelMx;
	modelMx = glm::translate(modelMx, glm::vec3(5.0f, 5.0f, 1.0f));
	modelMx = glm::rotate(modelMx, angleO, glm::vec3(0.0f, 0.0f, 1.0f));

	angleO -= 0.1f;

	return modelMx;
}

void Scene::drawObject(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx, GLuint glProgramId, bool useTexture)
{
	glUseProgram(glProgramId);

	GLint mmxLocation = glGetUniformLocation(glProgramId, "uModelMX");
	GLint viewmxLocation = glGetUniformLocation(glProgramId, "uViewMX");
	GLint projLocation = glGetUniformLocation(glProgramId, "uProjMX");
	GLint addColorLocation = glGetUniformLocation(glProgramId, "uAdditiveColor");

	glm::mat4x4 modelMx = getObjectMX();
	glUniformMatrix4fv(mmxLocation, 1, GL_FALSE, glm::value_ptr(modelMx));
	glUniformMatrix4fv(viewmxLocation, 1, GL_FALSE, glm::value_ptr(viewMx));
	glUniformMatrix4fv(projLocation, 1, GL_FALSE, glm::value_ptr(projMx));
	glUniform4f(addColorLocation, 0.9f, 0.5f, 0.2f, 1.0f);

	GLint error = glGetError();
	switch (error)
	{
	case GL_INVALID_VALUE:
		printf("GL_INVALID_VALUE\n");
		break;
	case GL_INVALID_OPERATION:
		printf("GL_INVALID_OPERATION\n");
		break;
	default:
		break;
	};

	glBindBuffer(GL_ARRAY_BUFFER, mObjectVertexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mObjectIndexBufferId);

    GLint vertLocation = glGetAttribLocation(glProgramId, "aPosition");
	glVertexAttribPointer(vertLocation, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	GLint texLocation = glGetAttribLocation(glProgramId, "aTextureCoord");
	if (useTexture)
	{
		uint64_t txOffset = offsetof(Vertex, tx);
		glVertexAttribPointer(texLocation, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void *)txOffset);
	}

	glEnableVertexAttribArray(vertLocation);
	if (useTexture)
		glEnableVertexAttribArray(texLocation);
	//
	glDrawElements(GL_TRIANGLE_STRIP, 6 * 6, GL_UNSIGNED_INT, 0);
	//
	glDisableVertexAttribArray(vertLocation);
	if (useTexture)
		glDisableVertexAttribArray(texLocation);

	checkGLError(__FUNCTION__);
}

void Scene::drawSurface(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx, GLuint glProgramId, bool useTexture)
{
	glUseProgram(glProgramId);

	GLint mmxLocation = glGetUniformLocation(glProgramId, "uModelMX");
	GLint viewmxLocation = glGetUniformLocation(glProgramId, "uViewMX");
	GLint projLocation = glGetUniformLocation(glProgramId, "uProjMX");
	GLint addColorLocation = glGetUniformLocation(glProgramId, "uAdditiveColor");

	glm::mat4x4 modelMx = getSurfaceMX();
	glUniformMatrix4fv(mmxLocation, 1, GL_FALSE, glm::value_ptr(modelMx));
	glUniformMatrix4fv(viewmxLocation, 1, GL_FALSE, glm::value_ptr(viewMx));
	glUniformMatrix4fv(projLocation, 1, GL_FALSE, glm::value_ptr(projMx));
	glUniform4f(addColorLocation, 0.1f, 0.8f, 0.4f, 1.0f);

	GLint error = glGetError();
	switch (error)
	{
	case GL_INVALID_VALUE:
		printf("GL_INVALID_VALUE\n");
		break;
	case GL_INVALID_OPERATION:
		printf("GL_INVALID_OPERATION\n");
		break;
	default:
		break;
	};

	glBindBuffer(GL_ARRAY_BUFFER, mSurfaceVertexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mSurfaceIndexBufferId);

    GLint vertLocation = glGetAttribLocation(glProgramId, "aPosition");
	glVertexAttribPointer(vertLocation, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

    GLint texLocation = glGetAttribLocation(glProgramId, "aTextureCoord");
	if (useTexture)
	{
		uint64_t txOffset = offsetof(Vertex, tx);
		glVertexAttribPointer(texLocation, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void *)txOffset);
	}

	glEnableVertexAttribArray(vertLocation);
	if (useTexture)
		glEnableVertexAttribArray(texLocation);
	//
	glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, 0);
	//
	glDisableVertexAttribArray(vertLocation);
	if (useTexture)
		glDisableVertexAttribArray(texLocation);

	checkGLError(__FUNCTION__);
}

void Scene::drawFinal(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx)
{
	glUseProgram(mProgramShadowId);

	GLint lightModelMxLoc = glGetUniformLocation(mProgramShadowId, "uLightModelMX");
	GLint lightViewMxLoc = glGetUniformLocation(mProgramShadowId, "uLightViewMX");
	GLint lightProjMxLoc = glGetUniformLocation(mProgramShadowId, "uLightProjMX");
	GLint ratioLoc = glGetUniformLocation(mProgramShadowId, "uDepthBufferRatio");

	glm::mat4x4 lightModelMx;
	glUniformMatrix4fv(lightModelMxLoc, 1, GL_FALSE, glm::value_ptr(lightModelMx));
	glUniformMatrix4fv(lightViewMxLoc, 1, GL_FALSE, glm::value_ptr(mLightViewMX));
	glUniformMatrix4fv(lightProjMxLoc, 1, GL_FALSE, glm::value_ptr(mLightProjMX));

	float txW = (float)mWidth / (float)sDepthBufDim;
	float txH = (float)mHeight / (float)sDepthBufDim;
	glUniform2f(ratioLoc, txW, txH);

	glBindTexture(GL_TEXTURE_2D, mFBTextureId);
	glActiveTexture(GL_TEXTURE0);

	drawObject(projMx, viewMx, mProgramShadowId, true);
	drawSurface(projMx, viewMx, mProgramShadowId, true);
}

std::string Scene::printLogForShader(GLint shaderId)
{
    int len = 0;

    int written = 0;
    char* info;

    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &len);

    if (len > 0) {
        info = (char*)malloc(len);
        glGetShaderInfoLog(shaderId, len, &written, info);

        std::string str = info;

        free(info);

        return str;
    }

    return "";
}

std::string Scene::readFile(const std::string & filePath)
{
	FILE * file = fopen(filePath.c_str(), "r");
	if (!file)
		return "";

	fseek(file, 0, SEEK_END);
	size_t size = ftell(file);
	fseek(file, 0, SEEK_SET);

	char *buff = static_cast<char *>(malloc(size * sizeof(unsigned char)));
	fread(buff, sizeof(unsigned char), size, file);
	fclose(file);

	std::string str(buff, size);
	free(buff);
//	printf("%s", str.c_str());
	return str;
}

void Scene::checkGLError(const char * callerFunk)
{
	GLenum err = glGetError();
	switch (err)
	{
	case GL_INVALID_ENUM:
		printf("\tGL_INVALID_ENUM %s\n", callerFunk);
		break;
	case GL_INVALID_VALUE:
		printf("\tGL_INVALID_VALUE %s\n", callerFunk);
		break;
	case GL_INVALID_OPERATION:
		printf("\tGL_INVALID_OPERATION %s\n", callerFunk);
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		printf("\tGL_INVALID_FRAMEBUFFER_OPERATION %s\n", callerFunk);
		break;
	case GL_OUT_OF_MEMORY:
		printf("\tGL_OUT_OF_MEMORY %s\n", callerFunk);
		break;
	case GL_STACK_UNDERFLOW:
		printf("\tGL_STACK_UNDERFLOW %s\n", callerFunk);
		break;
	case GL_STACK_OVERFLOW:
		printf("\tGL_STACK_OVERFLOW %s\n", callerFunk);
		break;
	case GL_NO_ERROR:
		break;
	default:
		printf("\tUnhandled GL_ERROR %s\n", callerFunk);
		break;
	}
}

void Scene::printPixel(float x, float y)
{
    glBindBuffer(GL_FRAMEBUFFER, mFramebufferId);

    GLubyte pixels[4] = { 0, 0, 0, 0 };
    glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

    printf("PIXEL AT %.f:%.f is R:%d G:%d B:%d A:%d\n", x, y, pixels[0], pixels[1], pixels[2], pixels[3]);

    glBindBuffer(GL_FRAMEBUFFER, 0);
}

#endif
