//
//  Scene.h
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#include <OpenGL/OpenGL.h>
#include <glm.hpp>
#include <string>

#ifndef Scene_h
#define Scene_h

class Scene
{
public:
	/// Первичная инициализация
	void init();
	/// Установить размер окна
	void setSize(int width, int height);

	/// Отрисовать сцену
	void draw();

private:
	/// Размер окна
	int mWidth = 0;
	int mHeight = 0;

	/// Матрица вида (для камеры)
	glm::mat4x4 mViewMX;
	/// Матрица вида для света
	glm::mat4x4 mLightViewMX;
	/// Матрица проекции (для камеры)
	glm::mat4x4 mProjMX;
	/// Матрица проекции для света
	glm::mat4x4 mLightProjMX;

	GLuint mProgramSimpleId = 0;
	GLuint mProgramAdvancedId = 0;
	GLuint mProgramShadowId = 0;
	/// VBO-id для поверхности
	GLuint mSurfaceVertexBufferId = 0;
	GLuint mSurfaceIndexBufferId = 0;
	/// VBO-id для объекта
	GLuint mObjectVertexBufferId = 0;
	GLuint mObjectIndexBufferId = 0;
	/// Первый рендер (для инициализации)
	bool mBoolFirstDraw = true;
	/// Фреймбуфер для отрисовки
	GLuint mFramebufferId = 0;
	/// Текстура фреймбуфера
	GLuint mFBTextureId = 0;
	/// Буфер глубины фреймбуфера
//	GLuint mFBDepthTexId = 0;
	/// VBO для отрисовки содержимого фреймбуфера
	GLuint mFBVBOId = 0;
	GLuint mFBVBOIntexId = 0;

	/// Создание вьюпорта
	void setupViewport();
	/// Создание простого шейдера
	void setupSimpleProgram();
	/// Создание шейдера с текстурингом
	void setupAdvancedProgram();
	/// Создение шейдера для отрисовки теней
	void setupShadowProgram();
	/// Создание кастомной шейдерной программы
	void setupProgram(const std::string & vertFilePath, const std::string & fragFilePath, GLuint * programId);
	/// Создание VBO поверхности
	void setupSurfaceVBO();
	/// Создание VBO объекта
	void setupObjectVBO();
	/// Создание фреймбуйера для отрисовки в него
	void setupFramebuffer();
	/// Отрисовка содержимого фреймбуфера
	void drawFramebufferContent();

	/// Получить матрицу модели поверхности
	glm::mat4x4 getSurfaceMX();
	/// Получить матрицу модели объекта
	glm::mat4x4 getObjectMX();
	/// Отрисовать объект
	void drawObject(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx, GLuint glProgramId, bool useTexture);
	/// Отрисовать поверхность
	void drawSurface(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx, GLuint glProgramId, bool useTexture);
	/// Отрисовать финальный вариант с тенями
	void drawFinal(const glm::mat4x4 & projMx, const glm::mat4x4 & viewMx);

	/// Распечатать ошибки при создании шейдера
	std::string printLogForShader(GLint shaderId);
	/// Прочитать файл в строку
	std::string readFile(const std::string & filePath);
	/// Обработать ошибку OpenGL
	void checkGLError(const char * callerFunk);
	/// Распечатать пиксель по координате
	void printPixel(float x, float y);
};

#endif /* Scene_h */
