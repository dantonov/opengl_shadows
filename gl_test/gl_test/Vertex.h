//
//  Vertex.h
//  gl_test
//
//  Created by Dmitry Antonov on 30/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#ifndef Vertex_h
#define Vertex_h

#pragma pack(push, 1)

struct Vertex
{
	inline Vertex() {}
	inline Vertex(const std::initializer_list<float> & list);

	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat z = 0.0f;

	GLfloat tx = 0.0f;
	GLfloat ty = 0.0f;
};

#pragma pack(pop)

Vertex::Vertex(const std::initializer_list<float> & list)
{
	int index = 0;
	for (const float & val: list)
	{
		if (index == 0)
			x = val;
		if (index == 1)
			y = val;
		if (index == 2)
			z = val;
		if (index == 3)
			tx = val;
		if (index == 4)
			ty = val;

		++index;
	}
}

#endif /* Vertex_h */
