//
//  ViewController.m
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

@synthesize glContext;
@synthesize glView;

- (void)viewDidLoad {
	[super viewDidLoad];

    NSOpenGLPixelFormatAttribute attributes[]
        = { NSOpenGLPFADoubleBuffer, NSOpenGLPFADepthSize, 24, NSOpenGLPFAStencilSize, 8, 0 };

    NSOpenGLPixelFormat* pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];

	glContext = [[[NSOpenGLContext alloc] init] autorelease];

	NSRect winRect = [self.view frame];
	glView = [[GLView alloc] initWithFrame:winRect pixelFormat:pixelFormat];

	[glContext setView: glView];
	[glContext makeCurrentContext];

	[self.view addSubview: glView];

	[glView initGL];
}

- (void)setRepresentedObject:(id)representedObject {
	[super setRepresentedObject:representedObject];

	// Update the view, if already loaded.
}

@end
