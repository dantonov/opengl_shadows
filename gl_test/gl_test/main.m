//
//  main.m
//  gl_test
//
//  Created by Dmitry Antonov on 29/09/16.
//  Copyright © 2016 Dmitry Antonov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"

int main(int argc, const char * argv[])
{
    NSApplication * app = [NSApplication sharedApplication];
	[app setDelegate: [[AppDelegate alloc] init]];

	NSApplicationMain(argc, argv);

    return 0;
}
