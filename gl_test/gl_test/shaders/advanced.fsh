//
uniform sampler2D uTexture0;

varying vec4 vAdditiveColor;
varying vec2 vTextureCoord;

void main()
{
	vec4 tex = texture2D(uTexture0, vTextureCoord);
    vec4 color = tex + vAdditiveColor;

	gl_FragColor = color;
}
