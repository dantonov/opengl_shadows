//
uniform mat4 uModelMX;
uniform mat4 uViewMX;
uniform mat4 uProjMX;

uniform vec4 uAdditiveColor;

attribute vec4 aPosition;
attribute vec2 aTextureCoord;

varying vec4 vAdditiveColor;
varying vec2 vTextureCoord;

void main()
{
	vAdditiveColor = uAdditiveColor;
	vTextureCoord = aTextureCoord;

	mat4 MVP = uProjMX * uViewMX * uModelMX;
	vec4 pos = vec4(aPosition.x, aPosition.y, aPosition.z, 0.0);
    gl_Position = MVP * aPosition;
}
