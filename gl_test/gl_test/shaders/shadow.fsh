//
uniform sampler2D uTexture0; /// Текстура с данными глубины от источника света

uniform vec2 uDepthBufferRatio; /// Отношение размера текстуры буфера глубины к размеру экрана

varying vec4 vAdditiveColor;
varying vec4 vFragPosLightSpace;

varying vec2 vTexCoords;

float calculateShadow(vec4 fpls)
{
	vec3 projCoords = fpls.xyz / fpls.w;
	projCoords = projCoords * 0.5 + 0.5;
	float closestDepth = texture2D(uTexture0, projCoords.xy * uDepthBufferRatio).r;
	float currentDepth = projCoords.z;
	float bias = 0.01;
	float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

	return shadow;
}

void main()
{
    vec4 color = vAdditiveColor;
//	vec4 tex = texture2D(uTexture0, vTexCoords);

	float shadow = calculateShadow(vFragPosLightSpace);
//	color.r = 1.0 - shadow;
//	color.g = 1.0 - shadow;
//	color.b = 1.0 - shadow;
	gl_FragColor = color - shadow * 0.5;
}
