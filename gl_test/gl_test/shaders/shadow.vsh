//
uniform mat4 uModelMX;
uniform mat4 uViewMX;
uniform mat4 uProjMX;

uniform mat4 uLightModelMX;
uniform mat4 uLightViewMX;
uniform mat4 uLightProjMX;

uniform vec4 uAdditiveColor;

attribute vec4 aPosition;
attribute vec2 aTextureCoord;

varying vec4 vAdditiveColor;
varying vec4 vFragPosLightSpace;
varying vec2 vTexCoords;

void main()
{
	vAdditiveColor = uAdditiveColor;
	vTexCoords = aTextureCoord;
	//
	mat4 MVP = uProjMX * uViewMX * uModelMX;
	vec4 pos = vec4(aPosition.xyz, 1.0);
    gl_Position = MVP * aPosition;
	//

	mat4 lightSpaceMx = uLightProjMX * uLightViewMX * uLightModelMX;
	vec3 fragPos = vec3(uModelMX * vec4(aPosition.xyz, 1.0));
	vFragPosLightSpace = lightSpaceMx * vec4(fragPos, 1.0);
}
