//
varying vec4 vAdditiveColor;

void main()
{
    vec4 color = vAdditiveColor;

	gl_FragColor = color;
}
